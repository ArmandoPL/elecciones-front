import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListadoComponent } from './components/listado/listado.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'consult-person',
    pathMatch: 'full'
  },
  {
    path: 'consult-person',
    component: ListadoComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
