import { Component, OnInit } from '@angular/core';


import { CoordinadorEstructuraGeneral } from 'src/app/models/coordinador-estructura-general.interface';
import { ConsultarPersonaService } from 'src/app/services/consultar-persona/consultar-persona.service';
import { ProspectoVotante } from 'src/app/models/prospecto-votante.interface';
import { Persona } from 'src/app/models/persona.interface';
import { Area } from 'src/app/models/area.interface';
import { RepresentanteGeneral } from 'src/app/models/representante-general.interface';
import { RepresentanteSeccional } from 'src/app/models/representante-seccional.interface';
import { RepresentanteCasilla } from 'src/app/models/representante-casilla.interface';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Direccion } from 'src/app/models/direccion.interface';
import Swal from 'sweetalert2';
import { BaseResponse } from 'src/app/models/base-response.model';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.scss']
})
export class ListadoComponent implements OnInit {
  loadingRepresentanteGeneral = false;
  loadingRepresentanteSeccional = false;
  loadingRespresentateCasilla = false;
  loadingProspectoVotante = false;
  coordinadorEstructuraElectoral: CoordinadorEstructuraGeneral;
  representanteGeneral: RepresentanteGeneral[];
  representanteSeccional: RepresentanteSeccional[];
  respresentateCasilla: RepresentanteCasilla[];
  prospectosVotantes: ProspectoVotante[];
  detallePersonaModal: Persona;
  detalleVotanteModal: ProspectoVotante;
  areaModal: Area;
  seccionRepresentante: number;
  personaFormulario: FormGroup;
  submitted = false;
  validacionPersonRegistro;
  personaAsignadaVotante: number;
  // tslint:disable-next-line: new-parens
  baseResponse = new BaseResponse;


  /**
   * Constructor del componente
   * @param personaService servicio para la consulta de personas
   */
  constructor(private personaService: ConsultarPersonaService,
    private formBuilder: FormBuilder) { }

  /**
   * Funcion inicial del componente
   */
  ngOnInit(): void {
    this.getCoordinadorEstructuraElectoral();
    this.createPersonaForm();
  }

  /**
   * Se crea el formulario para registrar personas
   */
  createPersonaForm(): void {
    this.personaFormulario = this.formBuilder.group({
      nombre: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-ZáéíóúñÁÉÍÓÚÑ ]*')])],
      appPaterno: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-ZáéíóúñÁÉÍÓÚÑ ]*')])],
      appMaterno: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-ZáéíóúñÁÉÍÓÚÑ ]*')])],
      usuarioFacebook: ['', Validators.compose([Validators.required])],
      correo: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])],
      telefono: ['', Validators.compose([Validators.required, Validators.minLength(8), Validators.pattern('^[0-9]{8,14}')])],
      calle: ['', Validators.compose([Validators.required])],
      numero: ['', Validators.compose([Validators.required])],
      colonia: ['', Validators.compose([Validators.required])],
      referencia: ['', Validators.compose([Validators.required])],
      idPersonaAsignada: ['-1', Validators.compose([Validators.required, Validators.pattern('^[0-9]+')])],
      numeroSeccion: ['', null],
    });
  }

  /**
   * Resetear el formulario de regsitro de personas
   */
  reset(): void {
    this.personaFormulario.reset();
    this.createPersonaForm();
  }

  /**
   * Se obtienen los controles del formulario de personas
   */
  get formControls(): any {
    return this.personaFormulario.controls;
  }

  /**
   * Se obtiene informacion del coordinador de estructura electoral
   */
  getCoordinadorEstructuraElectoral(): void {
    this.personaService.getCoordinadorEstructuraElectoral().subscribe(data => {
      this.baseResponse = (data as BaseResponse);
      if (this.baseResponse.code === 200) {
        this.coordinadorEstructuraElectoral = (this.baseResponse.responseListData as CoordinadorEstructuraGeneral);
        this.getRepresentanteGeneral(this.coordinadorEstructuraElectoral.idPersona.idPersona);
      } else {
        this.showAlertMessage(this.baseResponse.message);
      }
    }, error => {
      this.showAlertaError();
    });
  }

  /**
   * Se obtiene la lista de representante general asignado al coordinador electoral
   * @param idPersona id coordinador electoral
   */
  getRepresentanteGeneral(idPersona: number): void {
    this.loadingRepresentanteGeneral = true;
    this.representanteGeneral = null;
    this.personaService.getRepresentanteGeneral(idPersona).subscribe(data => {
      this.baseResponse = (data as BaseResponse);
      if (this.baseResponse.code === 200) {
        this.representanteGeneral = (this.baseResponse.responseListData as RepresentanteGeneral[]);
        this.loadingRepresentanteGeneral = false;
      } else {
        this.loadingRepresentanteGeneral = false;
        this.showAlertMessage(this.baseResponse.message);
      }
    }, error => {
      this.showAlertaError();
      this.loadingRepresentanteGeneral = false;

    });
  }

  /**
   * Se obtiene la lista de representante seccional asignado al representante general
   * @param idPersona id Representante General
   */
  getRepresentanteSeccional(idPersona: number): void {
    this.loadingRepresentanteSeccional = true;
    this.representanteSeccional = null;
    this.personaService.getRepresentanteSeccional(idPersona).subscribe(data => {
      this.baseResponse = (data as BaseResponse);
      if (this.baseResponse.code === 200) {
        this.representanteSeccional = (this.baseResponse.responseListData as RepresentanteSeccional[]);
        this.loadingRepresentanteSeccional = false;
      } else {
        this.showAlertMessage(this.baseResponse.message);
        this.loadingRepresentanteSeccional = false;
      }
    }, error => {
      this.showAlertaError();
      this.loadingRepresentanteSeccional = false;
    });
  }

  /**
   * Se obtiene la lista de representante de casilla asignado al representante seccional
   * @param idPersona id Representante seccional
   */
  getRepresentanteCasilla(idPersona: number): void {
    this.loadingRespresentateCasilla = true;
    this.respresentateCasilla = null;
    this.personaService.getRepresentanteCasilla(idPersona).subscribe(data => {
      this.baseResponse = (data as BaseResponse);
      if (this.baseResponse.code === 200) {
        this.respresentateCasilla = (this.baseResponse.responseListData as RepresentanteCasilla[]);
        this.loadingRespresentateCasilla = false;
      } else {
        this.showAlertMessage(this.baseResponse.message);
        this.loadingRespresentateCasilla = false;
      }
    }, error => {
      this.showAlertaError();
      this.loadingRespresentateCasilla = false;
    });
  }

  /**
   * Se obtienen los votantes asignados a una persona
   */
  getVotantes(idPersona: number): void {
    this.loadingProspectoVotante = true;
    this.prospectosVotantes = null;
    this.personaService.getProspectoVotante(idPersona).subscribe(data => {
      this.baseResponse = (data as BaseResponse);
      if (this.baseResponse.code === 200) {
        this.prospectosVotantes = (this.baseResponse.responseListData as ProspectoVotante[]);
        this.loadingProspectoVotante = false;
      }
    }, error => {
      this.showAlertaError();
      this.loadingProspectoVotante = false;
    });
  }

  /**
   * Se construye el detalle de el modal para el coordinador de estructura electoral
   */
  buildModalCoordinadorEstructuraElectoral(): void {
    this.detallePersonaModal = null;
    this.areaModal = null;
    this.seccionRepresentante = null;
    this.getVotantes(this.coordinadorEstructuraElectoral.idPersona.idPersona);
    this.detallePersonaModal = this.coordinadorEstructuraElectoral.idPersona;
    this.areaModal = this.coordinadorEstructuraElectoral.idArea;
  }

  /**
   * Se contruye el detalle del modal para el representante general
   * @param representanteGeneral detalle del representante general seleccionado
   */
  buildModalRepresentanteGeneral(representanteGeneral: RepresentanteGeneral): void {
    this.detallePersonaModal = null;
    this.areaModal = null;
    this.seccionRepresentante = null;
    this.getVotantes(representanteGeneral.idPersona.idPersona);
    this.detallePersonaModal = representanteGeneral.idPersona;
    this.areaModal = representanteGeneral.idArea;
  }

  /**
   * Se contruye el detalle del modal para el representante seccional
   * @param representanteSeccional detalle del representante seccional seleccionado
   */
  buildModalRepresentanteSeccional(representanteSeccional: RepresentanteSeccional): void {
    this.detallePersonaModal = null;
    this.areaModal = null;
    this.seccionRepresentante = null;
    this.getVotantes(representanteSeccional.idPersona.idPersona);
    this.detallePersonaModal = representanteSeccional.idPersona;
    this.areaModal = representanteSeccional.idArea;
    this.seccionRepresentante = representanteSeccional.numeroSeccion;
  }

  /**
   * Se construte el detalle del modal para el representante de casilla
   * @param representanteCasilla detalle del representante de casilla seleccinado
   */
  buildModalRepresentanteCasilla(representanteCasilla: RepresentanteCasilla): void {
    this.detallePersonaModal = null;
    this.areaModal = null;
    this.seccionRepresentante = null;
    this.getVotantes(representanteCasilla.idPersona.idPersona);
    this.detallePersonaModal = representanteCasilla.idPersona;
    this.areaModal = representanteCasilla.idArea;
  }

  /**
   * Se construye el detalle de el modal de votantes
   * @param detailPerson detalle del votante
   */
  buildVotantesModal(detailPerson: ProspectoVotante): void {
    this.detalleVotanteModal = detailPerson;
  }

  /**
   * Metodo para registrar a una persona
   */
  validateForm(): void {
    this.submitted = true;
    if (this.personaFormulario.invalid) {
      return;
    } else {
      this.buildPeticionRegistroPersona();
    }
  }

  /**
   * Se comienza el registro de un votante
   * @param idPersonaAsignada persona a la que esta asigado el votante
   */
  startRegistroVotante(idPersonaAsignada: number): void {
    console.log('Se comienza el registro de votante');
    this.reset();
    this.validacionPersonRegistro = 4;
    this.personaAsignadaVotante = idPersonaAsignada;
  }

  /**
   * Se construye la peticion para realizar el registro de personas
   */
  buildPeticionRegistroPersona(): void {
    switch (this.validacionPersonRegistro) {
      case 1:
        this.registrarRepresentanteGeneral();
        break;
      case 2:
        this.registarRepresentanteSeccional();
        break;

      case 3:
        this.registrarRepresentanteCasilla();
        break;

      case 4:
        this.registrarVotante();
        break;

      default:
        console.log('Opcion no valida');
        break;
    }
  }

  /**
   * Se registra representane general
   */
  registrarRepresentanteGeneral(): void {
    const representanteGeneralRequest = (this.buildPersonaRegistroRequest() as RepresentanteGeneral);
    this.personaService.createRepresentanteGeneral(representanteGeneralRequest).subscribe(data => {
      this.baseResponse = (data as BaseResponse);
      if (this.baseResponse.code === 200) {
        this.showAlertaExito(this.baseResponse.message);
        this.getRepresentanteGeneral(representanteGeneralRequest.idCoordinadorEstructuraGeneral.idCoordinadorEstructuraGeneral);
      } else {
        this.showAlertMessage(this.baseResponse.message);
      }
    }, error => {
      this.showAlertaError();
    });
  }

  /**
   * Se registra representante seccional
   */
  registarRepresentanteSeccional(): void {
    const representanteSeccionaRequest = (this.buildPersonaRegistroRequest() as RepresentanteSeccional);
    this.personaService.createRepresentanteSeccional(representanteSeccionaRequest).subscribe(data => {
      this.baseResponse = (data as BaseResponse);
      if (this.baseResponse.code === 200) {
        this.showAlertaExito(this.baseResponse.message);
        this.getRepresentanteSeccional(representanteSeccionaRequest.idRepresentanteGeneral.idRepresentanteGeneral);
      } else {
        this.showAlertMessage(this.baseResponse.message);
      }
    }, error => {
      this.showAlertaError();
    });
  }

  /**
   * Se registra representante de casillas
   */
  registrarRepresentanteCasilla(): void {
    const representanteCasillaRequest = (this.buildPersonaRegistroRequest() as RepresentanteCasilla);
    this.personaService.createRepresentanteCasilla(representanteCasillaRequest).subscribe(data => {
      this.baseResponse = (data as BaseResponse);
      if (this.baseResponse.code === 200) {
        this.showAlertaExito(this.baseResponse.message);
        this.getRepresentanteCasilla(representanteCasillaRequest.idRepresentanteSeccional.idRepresentanteSeccional);
      } else {
        this.showAlertMessage(this.baseResponse.message);
      }
    }, error => {
      this.showAlertaError();
    });
  }

  /**
   * Se registra votante asignado a un representante
   */
  registrarVotante(): void {
    const votanteRequest = (this.buildPersonaRegistroRequest() as ProspectoVotante);
    this.personaService.createProspectoVotante(votanteRequest).subscribe(data => {
      this.baseResponse = (data as BaseResponse);
      if (this.baseResponse.code === 200) {
        this.showAlertaExito(this.baseResponse.message);
        this.getVotantes(votanteRequest.idPersonaAsignada.idPersona);
      } else {
        this.showAlertMessage(this.baseResponse.message);
      }
    }, error => {
      this.showAlertaError();
    });
  }

  /**
   * Se construye la peticion la el registro de representantes
   */
  buildPersonaRegistroRequest(): RepresentanteGeneral | RepresentanteSeccional | RepresentanteCasilla | ProspectoVotante {
    let representante: RepresentanteGeneral | RepresentanteSeccional | RepresentanteCasilla | ProspectoVotante;

    switch (this.validacionPersonRegistro) {
      case 1:
        representante = {
          idPersona: this.buildPersona(),
          idCoordinadorEstructuraGeneral: {
            idCoordinadorEstructuraGeneral: this.personaFormulario.value.idPersonaAsignada
          }
        } as RepresentanteGeneral;
        break;

      case 2:
        representante = {
          idPersona: this.buildPersona(),
          numeroSeccion: this.personaFormulario.value.numeroSeccion,
          idRepresentanteGeneral: {
            idRepresentanteGeneral: this.personaFormulario.value.idPersonaAsignada
          }
        } as RepresentanteSeccional;
        break;

      case 3:
        representante = {
          idPersona: this.buildPersona(),
          idRepresentanteSeccional: {
            idRepresentanteSeccional: this.personaFormulario.value.idPersonaAsignada
          }
        } as RepresentanteCasilla;
        break;

      case 4:
        representante = {
          idPersona: this.buildPersona(),
          idPersonaAsignada: {
            idPersona: this.personaAsignadaVotante
          }
        } as ProspectoVotante;
        break;

      default:
        console.log('Opcion no valida');
        break;
    }
    return representante;
  }

  /**
   * Se arma cuerpo de peticion para una persona
   */
  buildPersona(): Persona {
    const persona = {
      nombre: this.personaFormulario.value.nombre,
      appPaterno: this.personaFormulario.value.appPaterno,
      appMaterno: this.personaFormulario.value.appMaterno,
      usuarioFacebook: this.personaFormulario.value.usuarioFacebook,
      correo: this.personaFormulario.value.correo,
      telefono: this.personaFormulario.value.telefono,
      idDireccion: this.buildDireccion()
    } as Persona;
    return persona;
  }

  /**
   * Se arma cuerpo de peticion de una direcciona
   */
  buildDireccion(): Direccion {
    const direccion = {
      calle: this.personaFormulario.value.calle,
      numero: this.personaFormulario.value.numero,
      colonia: this.personaFormulario.value.colonia,
      referencia: this.personaFormulario.value.referencia
    } as Direccion;
    return direccion;
  }

  /**
   * Se muestra alerta de error al realizar una accion
   */
  showAlertaError(): void {
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Ha ocurrido un error!',
    });
  }

  /**
   * Se muestra alerta de exito al realizar una accion
   */
  showAlertaExito(message: string): void {
    Swal.fire({
      icon: 'success',
      title: message
    });
  }

  /**
   * Se muestra alerta con un mensaje
   * @param message Mensaje que se muestra
   */
  showAlertMessage(message: string): void {
    Swal.fire({
      icon: 'warning',
      title: message
    });
  }
}
