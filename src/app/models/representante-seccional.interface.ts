import { Persona } from './persona.interface';
import { Area } from './area.interface';
import { RepresentanteGeneral } from './representante-general.interface';

export interface RepresentanteSeccional {
    idRepresentanteSeccional: number;
    numeroSeccion?: number;
    idPersona?: Persona;
    idArea?: Area;
    idRepresentanteGeneral?: RepresentanteGeneral;
}
