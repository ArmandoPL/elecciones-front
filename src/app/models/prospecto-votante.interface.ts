import { Persona } from "./persona.interface";

export interface ProspectoVotante {
    idProspecto: number;
    idPersona?: Persona;
    idPersonaAsignada?: Persona;
}
