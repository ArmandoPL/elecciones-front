import { Persona } from './persona.interface';
import { Area } from './area.interface';
import { CoordinadorEstructuraGeneral } from './coordinador-estructura-general.interface';

export interface RepresentanteGeneral {
    idRepresentanteGeneral: number;
    idPersona?: Persona;
    idArea?: Area;
    idCoordinadorEstructuraGeneral: CoordinadorEstructuraGeneral;
}
