export interface Area {
    idArea: number;
    nombre?: string;
    descripcion?: string;
}
