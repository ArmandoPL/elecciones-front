import { Persona } from './persona.interface';
import { Area } from './area.interface';

export interface CoordinadorEstructuraGeneral {
    idCoordinadorEstructuraGeneral: number;
    idPersona?: Persona;
    idArea?: Area;
    totalRepresentanteGeneral: number;
    totalRepresentanteSeccional: number;
    totalRepresentanteCasilla: number;
}
