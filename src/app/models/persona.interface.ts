import { Direccion } from './direccion.interface';

export interface Persona {
    idPersona: number;
    nombre?: string;
    appPaterno?: string;
    appMaterno?: string;
    usuarioFacebook?: string;
    correo?: string;
    telefono?: string;
    idDireccion?: Direccion;
}
