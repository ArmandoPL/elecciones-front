import { BaseModel } from './base.model';

export class BaseResponse {
	
	private _code: number;
	
	private _message: string;
	
	private _responseData: any;

	private _responseListData: any;

	private _complemetFlag: number;

	get code(): number {
		return this._code;
	}

	set code(_code: number) {
		this._code = _code;
	}

	get message(): string {
		return this._message;
	}

	set message(_message: string) {
		this._message = _message;
	}

	get responseData(): BaseModel {
		return this._responseData;
	}

	set responseData(_responseData: BaseModel) {
		this._responseData = _responseData;
	}

	get responseListData(): any {
		return this._responseListData;
	}

	set responseListData(_responseListData: any) {
		this._responseListData = _responseListData;
	}
	get complemetFlag(): number {
		return this._complemetFlag;
	}

	set complemetFlag(_complemetFlag: number) {
		this._complemetFlag = _complemetFlag;
	}

}