export interface Direccion {
    idDireccion: number;
    calle?: string;
    numero?: string;
    colonia?: string;
    referencia?: string;
}
