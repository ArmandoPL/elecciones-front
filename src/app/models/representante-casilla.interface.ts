import { Area } from './area.interface';
import { Persona } from './persona.interface';
import { RepresentanteSeccional } from './representante-seccional.interface';

export interface RepresentanteCasilla {
    idRepresentanteCasilla: number;
    idArea?: Area;
    idPersona?: Persona;
    idRepresentanteSeccional?: RepresentanteSeccional;
}
