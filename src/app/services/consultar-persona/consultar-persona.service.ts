import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { Observable } from 'rxjs';

import { CoordinadorEstructuraGeneral } from 'src/app/models/coordinador-estructura-general.interface';
import { ProspectoVotante } from 'src/app/models/prospecto-votante.interface';
import { RepresentanteGeneral } from 'src/app/models/representante-general.interface';
import { RepresentanteSeccional } from 'src/app/models/representante-seccional.interface';
import { RepresentanteCasilla } from 'src/app/models/representante-casilla.interface';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { HttpClientResponseService } from '../http-response/http-client-response.service';
import { BaseResponse } from 'src/app/models/base-response.model';

@Injectable({
  providedIn: 'root'
})
export class ConsultarPersonaService {

  apiEndPoint = 'http://localhost:8080/';

  constructor(private httpClient: HttpClient, private httpResponseService: HttpClientResponseService) { }

  /**
   * Se obtiene informacion del coordinador de estructura electoral
   */
  public getCoordinadorEstructuraElectoral(): Observable<BaseResponse> {
    return this.httpClient.get<BaseResponse>(this.apiEndPoint + 'Coordinador/',
      { headers: this.httpResponseService.cabeceras() });
  }

  /**
   * Se obtiene la lista de representante general
   * @param idPerson id coordinador electoral
   */
  public getRepresentanteGeneral(idPersona: number): Observable<BaseResponse> {
    return this.httpClient.get<BaseResponse>(this.apiEndPoint + 'representanteGral/' + idPersona,
      { headers: this.httpResponseService.cabeceras() });
  }

  /**
   * Se obtiene la lista de representante seccional
   * @param idPersona id Representante General
   */
  public getRepresentanteSeccional(idPersona: number): Observable<BaseResponse> {
    return this.httpClient.get<BaseResponse>(this.apiEndPoint + 'representante-seccional/' + idPersona,
      { headers: this.httpResponseService.cabeceras() });
  }

  /**
   * Se obtiene la lista de representante Casilla
   * @param idPersona id Representante seccional
   */
  public getRepresentanteCasilla(idPersona: number): Observable<BaseResponse> {
    return this.httpClient.get<BaseResponse>(this.apiEndPoint + 'representante-casilla/ ' + idPersona,
      { headers: this.httpResponseService.cabeceras() });
  }

  /**
   * Se obtiene el detalle de las personas votantes asignadas a una persona
   * @param idPersona Id de la persona asignada
   */
  public getProspectoVotante(idPersona: number): Observable<BaseResponse> {
    return this.httpClient.get<BaseResponse>(this.apiEndPoint + 'prospecto-votante/ ' + idPersona,
      { headers: this.httpResponseService.cabeceras() });
  }

  /**
   * Se realiza el registro o actualizacion de un representante general
   * @param representanteGeneral objeto de representante general
   */
  public createRepresentanteGeneral(representanteGeneral: RepresentanteGeneral): Observable<BaseResponse> {
    return this.httpClient.post<BaseResponse>(this.apiEndPoint + 'representanteGral/', representanteGeneral);
  }

  /**
   * Se realiza el registro o actualizacion de un representante seccional
   * @param representanteSeccional objeto de representante seccional
   */
  public createRepresentanteSeccional(representanteSeccional: RepresentanteSeccional): Observable<BaseResponse> {
    return this.httpClient.post<BaseResponse>(this.apiEndPoint + 'representante-seccional/', representanteSeccional);
  }

  /**
   * Se realiza el registro o actualizacion de un representante de casilla
   * @param representanteCasilla objeto de representante casilla
   */
  public createRepresentanteCasilla(representanteCasilla: RepresentanteCasilla): Observable<BaseResponse> {
    return this.httpClient.post<BaseResponse>(this.apiEndPoint + 'representante-casilla/', representanteCasilla);
  }

  /**
   * Se realiza el registro o actualizacion de un prospecto votante
   * @param prospectoVotantes onjeto de prospecto votante
   */
  public createProspectoVotante(prospectoVotantes: ProspectoVotante): Observable<BaseResponse> {
    return this.httpClient.post<BaseResponse>(this.apiEndPoint + 'prospecto-votante/', prospectoVotantes);
  }
}
