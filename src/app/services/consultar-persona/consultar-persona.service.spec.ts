import { TestBed } from '@angular/core/testing';

import { ConsultarPersonaService } from './consultar-persona.service';

describe('ConsultarPersonaService', () => {
  let service: ConsultarPersonaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConsultarPersonaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
