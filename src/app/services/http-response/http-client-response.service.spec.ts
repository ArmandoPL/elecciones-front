import { TestBed } from '@angular/core/testing';

import { HttpClientResponseService } from './http-client-response.service';

describe('HttpClientResponseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HttpClientResponseService = TestBed.get(HttpClientResponseService);
    expect(service).toBeTruthy();
  });
});
